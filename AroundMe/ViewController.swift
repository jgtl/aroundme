//
//  ViewController.swift
//  AroundMe
//
//  Created by Joseph Leichter on 7/10/18.
//  Copyright © 2018 Joseph Leichter. All rights reserved.
//

import UIKit
import MapKit
import CoreData
import CoreLocation

struct Loc {
    var latitude: Double
    var longitude: Double
}

class MyPointAnnotation : MKPointAnnotation {
    var pinTintColor: UIColor?
}

class ViewController: UIViewController {
    let locationManager = CLLocationManager()
    var locationsCoreDataArray:[NSManagedObject] = []
    var numLocations = 0
    let MAX_NUM_LOCATIONS = 3
    var locPointsToDisplay:[Loc] = []
    let regionRadius: CLLocationDistance = 100
    
    var currentLatitude:Double? = 0.0
    var currentLongitude:Double? = 0.0
    
    var storedLocationsShown:Bool = false
    var mapIsLoaded = false
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestAlwaysAuthorization()

        locationManager.delegate = self
        locationManager.stopUpdatingLocation()

    }
//let initialLocation = CLLocation(latitude: 21.282778, longitude: -157.829444)
    @IBOutlet weak var mapView: MKMapView!
    
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius, regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    

    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        

        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
       
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "Location")
        
 
        do {
           locationsCoreDataArray = try managedContext.fetch(fetchRequest)
          
           locPointsToDisplay = retrieveRecentLocations()
            
           
            for p in locPointsToDisplay{
                print("Latitude RETRIEVED: \(p.latitude)")
                print("Longitde RETRIEVED: \(p.longitude)\n")
            }
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
       
    }
    
    override func viewDidLayoutSubviews() {
        mapIsLoaded = true
        self.showLocs()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 10) { 
            
            self.locationManager.startUpdatingLocation()
        }
    }
  
}

extension ViewController : CLLocationManagerDelegate{
    
   
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last! //CLocationCoordinate2D
        //print("Location: \(location)")
     
        
        let latitude:Double = location.coordinate.latitude
        let longitude: Double = location.coordinate.longitude
        
        self.currentLatitude = latitude
        self.currentLongitude = longitude
        print("CURRENT LAT \(latitude)")
        print("CURRENT LONG \(longitude)")
        
       
        self.save(lat: latitude, long: longitude)
        
        if(mapIsLoaded){
            recenterMap()
        }


    }
    func showLocs(){
        if(!self.storedLocationsShown){
            self.showStoredLocations()
            self.storedLocationsShown = true
        }
        
        print("showLocsCalled")
    }
    
    
    func showStoredLocations(){
        
        
        print("Show stored locations called")
        if locPointsToDisplay.count > 0 {
            
            let centerLoc = locPointsToDisplay.popLast()
            self.centerMapOnLocation(location:CLLocation(latitude: (centerLoc?.latitude)!, longitude: (centerLoc?.longitude)!))
            
            let annotation = MyPointAnnotation()
            annotation.pinTintColor = .blue
            annotation.title = "Previous Location"
            
            annotation.coordinate = CLLocationCoordinate2D(latitude: (centerLoc?.latitude)!, longitude: (centerLoc?.longitude)!)
           
            mapView.addAnnotation(annotation)
            
            
            for loc in locPointsToDisplay{
                
                let annotation = MyPointAnnotation()
                annotation.pinTintColor = .blue
                annotation.title = "Previous Location"
                annotation.coordinate = CLLocationCoordinate2D(latitude: loc.latitude, longitude: loc.longitude)
                mapView.addAnnotation(annotation)
                
            }

            
        }else{
            
            self.recenterMap()
            
        }
        
       
       
    }
    
    func recenterMap(){
        self.centerMapOnLocation(location:CLLocation(latitude: self.currentLatitude!, longitude:
            self.currentLongitude!))
        
        let annotation = MyPointAnnotation()
        annotation.pinTintColor = .red
        annotation.title="Current Location"
        annotation.coordinate = CLLocationCoordinate2D(latitude:self.currentLatitude!, longitude:self.currentLongitude!)
        mapView.addAnnotation(annotation)
    }
    
 
    
    
    func retrieveRecentLocations() -> [Loc] {
        var locationsToReturn:[Loc] = []
        
        if locationsCoreDataArray.count > 0{
            
            for loc in locationsCoreDataArray{//[NSManagedObject]
                //get lat an long and make a struct
                let locStruct = Loc(latitude:loc.value(forKeyPath: "latitude") as! Double, longitude: loc.value(forKey: "longitude") as! Double)
                
                //add struct to array
                locationsToReturn.append(locStruct)
                
                //return [Loc]
            }//endfor
        
        }//endif
        
      return locationsToReturn
    }
    
    
    
    func save(lat: Double, long: Double) {
    
        if numLocations > MAX_NUM_LOCATIONS{
            return
        }
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
     
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        let entity =
            NSEntityDescription.entity(forEntityName: "Location",
                                       in: managedContext)!
        
        let location = NSManagedObject(entity: entity,
                                     insertInto: managedContext)
        
       
        location.setValue(lat, forKeyPath: "latitude")
        location.setValue(long, forKeyPath: "longitude")
      
        do {
            if locationsCoreDataArray.count==MAX_NUM_LOCATIONS{
                try managedContext.save()
                
                let f =  locationsCoreDataArray.removeFirst()
                managedContext.delete(f)
                locationsCoreDataArray.append(location)
                try managedContext.save()
                
            }else if locationsCoreDataArray.count==0{
                
                try managedContext.save()
        
                locationsCoreDataArray.append(location)
                numLocations+=1
                
            }else{
                try managedContext.save()
                locationsCoreDataArray.append(location)
                numLocations+=1
            }
            
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func clearAll(){
        
        //1
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "Location")
        
        //3
        do {
            locationsCoreDataArray = try managedContext.fetch(fetchRequest)
  
            for object in locationsCoreDataArray {
                managedContext.delete(object)
            }
            
            try managedContext.save()
            locationsCoreDataArray.removeAll()
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    
    
    }

}
extension ViewController: MKMapViewDelegate {

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "myAnnotation") as? MKPinAnnotationView
        
        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "myAnnotation")
            annotationView!.canShowCallout = true
            annotationView?.rightCalloutAccessoryView = UIButton(type: .infoLight)
            
        } else {
            annotationView?.annotation = annotation
        }
        
        if let annotation = annotation as? MyPointAnnotation {
            annotationView?.pinTintColor = annotation.pinTintColor
        }
        
        return annotationView
    }
    
    
    
    //The plan is to reverse geocode the selected pin
    //and display that in an alert. Sadly,
    //the method "calloutAccessoryControlTapped"
    //doesn't seem to be getting called when the user clicks
    //on the info button
    
    func mapView(mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        
       let lat =  view.annotation?.coordinate.latitude
       let long = view.annotation?.coordinate.longitude
       
        
        print("Hic est")
    }
    
    ////////////
    
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    print(pm.country)
                    print(pm.locality)
                    print(pm.subLocality)
                    print(pm.thoroughfare)
                    print(pm.postalCode)
                    print(pm.subThoroughfare)
                    var addressString : String = ""
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    
                    
                    print(addressString)
                }
        })
        
    }
    ///////////
    
    
    
}
